import { BrowserRouter, Routes, Route } from "react-router-dom";
import {UserPageProvider} from "pages/User/UserPageContext";
import './App.css';

function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<UserPageProvider pageCode={1} />}>
        <Route path="users" element={<UserPageProvider pageCode={1}  />} />
      </Route>
    </Routes>
  </BrowserRouter>
  );
}

export default App;
