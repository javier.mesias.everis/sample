
import { createContext, useCallback, useEffect, useState, ChangeEvent, useMemo, useRef } from "react";
import {UserPage} from "pages/User/UserPage"
import {getUsersByFilters} from "services/User/getUsers"
import {UserAddapter} from "adapters/User/UserAdapter"
import {UsuarioArray} from "models/User/User"

type UserProps = {
  users: UsuarioArray;

}

type PageProps = {
  pageCode: number
}

export const UserPageContext = createContext<UserProps>(
  {} as UserProps
)

export const UserPageProvider = ({ pageCode }: PageProps) => {
  const [users, setUsers] = useState<UsuarioArray>({content:[]});
  const firstTime = useRef(true)


      const setAllData = useCallback(
        async () => {
            const response = await getUsersByFilters({}).then(result => {
                const usersClean = UserAddapter(result) 
                setUsers(usersClean)
            })

          }
        ,
        [ users],
      )

      useEffect(() => {
      
        if (firstTime.current) {
          firstTime.current = false
          setAllData()   
        }
        
      }, [setAllData])

      const userContextData = useMemo(() => ({
        users
      }), [
        users
      ])
    
      return (
        <UserPageContext.Provider value={userContextData}>
          <UserPage />
        </UserPageContext.Provider>
      );
  }