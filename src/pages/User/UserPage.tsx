import {UserPageContext} from "pages/User/UserPageContext"
import { useState, useContext } from "react";
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));
  
  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

export const UserPage = () => {

    const {
        users
      } = useContext(UserPageContext);

      const rows: any = users.content

    return (
        <>
            <Table sx={{ width: 650 }} size="small" aria-label="a dense table">
                <TableHead>
                <TableRow>
                    <StyledTableCell>Nombre</StyledTableCell>
                    <StyledTableCell align="right">Apellido</StyledTableCell>
                    <StyledTableCell align="right">Empresa</StyledTableCell>
                    <StyledTableCell align="right">Nivel</StyledTableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                {rows.map((row:any) => (
                    <StyledTableRow key={row.name}>
                    <StyledTableCell component="th" scope="row">
                      {row.name}
                    </StyledTableCell>
                    <StyledTableCell align="right">{row.lastname}</StyledTableCell>
                    <StyledTableCell align="right">{row.company}</StyledTableCell>
                    <StyledTableCell align="right">{row.level}</StyledTableCell>
                    
                  </StyledTableRow>
                ))}
                </TableBody>
            </Table>
        </>
      );
}