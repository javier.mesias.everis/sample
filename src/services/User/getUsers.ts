
import axios from "axios";
import {Usuario} from "models/User/UserResponse"

export const getUsersByFilters = async ({ }: any): Promise<Usuario[]> => {
    const config = {
        headers: {},
        params: {
        }
    }

    const { data } = await axios.get<Usuario[]>('https://mocki.io/v1/fb7bfa68-1588-4513-b78e-96246b30964c', config);
    
    return data

}