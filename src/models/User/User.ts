export interface User {
    id: string;
    name: string;
    lastname: string;
    company: string;
    level: number;
}
export interface UsuarioArray {
    content:User[]
}
