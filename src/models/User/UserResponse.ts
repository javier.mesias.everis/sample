export interface Usuario {
    codigo: string;
    nombre: string;
    apellido: string;
    empresa: string;
    nivel: number;
}


