import {Usuario} from "models/User/UserResponse"
import {UsuarioArray} from "models/User/User"

export const UserAddapter = (users: Usuario[]): UsuarioArray => {

    let ret : UsuarioArray = {content:[]}
    
    for (let us of users) {
       ret.content.push({
            id: us.codigo,
            name: us.nombre,
            lastname: us.apellido,
            company:us.empresa,
            level:us.nivel
       })
    }

    return ret
}